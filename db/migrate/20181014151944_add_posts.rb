class AddPosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title, null: false, limit: 500
      t.text :body, null: false
      t.timestamps
    end
  end
end
