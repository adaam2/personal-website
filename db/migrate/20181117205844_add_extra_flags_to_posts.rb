class AddExtraFlagsToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :is_published, :boolean, default: false, null: false

    execute <<-SQL
      UPDATE posts SET is_published = 't';
    SQL
  end
end
