Post.delete_all

10.times do |i|
  Post.create!(
    title: Faker::GreekPhilosophers.unique.quote,
    body: Faker::Hacker.unique.say_something_smart,
    is_published: true
  )
end
