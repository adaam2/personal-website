process.env.NODE_ENV = process.env.NODE_ENV || "development";
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const webpack = require("webpack");
const environment = require("./environment");

environment.plugins.append(
  "Cut some cheese",
  new webpack.DefinePlugin({
    "process.env.NODE_ENV": "\"production\""
  })
);

environment.plugins.append(
  "BundleAnalyzer",
  new BundleAnalyzerPlugin({
    openAnalyzer: false,
    analyzerMode: "static",
    reportFilename: "./analyzer/report.html"
  })
);

module.exports = environment.toWebpackConfig();
