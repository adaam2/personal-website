const { environment } = require("@rails/webpacker");
const webpack = require("webpack");

environment.plugins.prepend(
  "MomentLocale",
  new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en|gb/)
);

module.exports = environment;
