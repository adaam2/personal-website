require 'sidekiq/web'

Rails.application.routes.draw do
  root to: 'app#index'

  namespace :api do
    get '/', to: 'home#index'

    namespace :v1 do
      resources :auth, only: [] do
        collection do
          post 'sign_in', to: 'auth/sign_in#authenticate'
        end
      end

      resources :users, only: [] do
        collection do
          get 'me', to: 'users#me'
        end
      end

      resources :posts, param: :slug, only: [:show, :index]

      namespace :admin do
        resources :posts
      end
    end
  end

  # Sidekiq UI
  # mount Sidekiq::Web => '/sidekiq'

  get '*path', to: 'app#index'
end
