source 'https://rubygems.org'

ruby '2.5.1'

gem 'rails', '~> 5.2.1'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'webpacker'
gem 'sidekiq'
gem 'active_model_serializers', '~> 0.10.0'
gem 'rollbar'
gem 'jwt'
gem 'bcrypt', '~> 3.1.7'
gem 'simple_command'

# Admin platform
gem 'trestle'
gem 'trestle-simplemde'
gem 'trestle-auth'

group :development, :test do
  gem 'pry-rails'
  gem 'dotenv-rails'
  gem 'factory_bot'
  gem 'factory_bot_rails'
  gem 'faker'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'rspec', '~> 3.8.0'
  gem 'rspec-rails'
  gem 'rspec_junit_formatter'
  gem 'capybara'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers', '4.0.0.rc1'
  gem 'simplecov'
  gem 'simplecov-console'
end
