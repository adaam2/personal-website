FactoryBot.define do
  factory :user do
    name { Faker::FunnyName.name }
    email { Faker::Internet.email }
    password { 'butter' }
    password_confirmation { 'butter' }
  end
end
