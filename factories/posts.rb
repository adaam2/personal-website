FactoryBot.define do
  factory :post do
    title { Faker::Hipster.sentence }
    body { Faker::Hipster.paragraph }
    created_at { Faker::Date.between(6.months.ago, Date.today) }
    slug { title&.parameterize }
    is_published { true }

    trait :invalid do
      title { nil }
    end

    trait :published do
      is_published { true }
    end

    trait :unpublished do
      is_published { false }
    end
  end
end
