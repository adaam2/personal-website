import React from "react";
import PropTypes from "prop-types";

import withPageTitle from "../shared/hoc/withPageTitle";

import mbinteractive from "./images/mbinteractive.png";
import farmdrop from "./images/farmdrop.png";
import sofar from "./images/sofar.png";
import route1 from "./images/route1.png";

import styles from "./cv.module.scss";

const Container = ({ setPageTitle }) => {
  setPageTitle("CV");

  return (
    <div id="cv">
      <div className={styles.section} data-section="employment-history">
        <h2>Preamble</h2>

        <p>
          I've been a developer for coming up to 6 years now. I've worked across
          multiple programming languages (so far my backend repertoire includes
          C#, Ruby, Python, VB.NET [eurgh], and Node) and across the stack (I am
          currently working as a full stack Ruby on Rails and React developer).
        </p>

        <p>
          I am always looking to grow my knowledge and find new opportunities
          that will advance my career and make me a better developer. I am
          currently on the lookout for any opportunities that will allow me to
          further develop my ES6 JavaScript experience. If you know of any roles
          that will allow me to practice some TypeScript as well as continuing
          to work on the backend in the form of a Rails or Node.js API then
          please do get in touch.
        </p>

        <p>
          <strong>FYI</strong>; I tend to respond better to <em>in-house</em>{" "}
          recruiters because it is really hard to see the wood from the trees
          with all of the recruiter emails received daily. No offence intended,
          but if you don't get a response, please don't email again.
        </p>

        <br />

        <h2>Recent Experience</h2>

        <ul className={styles.jobsList}>
          <li className={styles.jobWithLogo}>
            <span className={styles.logo}>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.sofarsounds.com"
              >
                <img src={sofar} alt="Sofar Sounds" />
              </a>
            </span>

            <span className={styles.mainJobContent}>
              <p className={styles.jobTitle}>
                <strong>Full Stack Developer</strong> <em>Sofar Sounds</em> -
                March 2018 - Present
              </p>
              <p className={styles.smallerDetail}>
                <em>Primary tech:</em> Ruby on Rails, Postgresql, Sidekiq,
                RSpec, Cucumber, ES6 React, Webpack 4, Python 3
              </p>
            </span>
          </li>

          <li className={styles.jobWithLogo}>
            <span className={styles.logo}>
              <a
                href="https://farmdrop.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={farmdrop} alt="Farmdrop" />
              </a>
            </span>

            <span className={styles.mainJobContent}>
              <p className={styles.jobTitle}>
                <strong>Full Stack Engineer</strong> <em>Farmdrop</em> - March
                2017 - March 2018
              </p>

              <p className={styles.smallerDetail}>
                <em>Primary tech:</em> Ruby on Rails, Postgresql, Kubernetes,
                Docker, RSpec, Cucumber, ES6 React, Webpack 4, Spree / Solidus,
                dry-rb
              </p>
            </span>
          </li>

          <li className={styles.jobWithLogo}>
            <span className={styles.logo}>
              <a
                href="https://route1.co"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img className={styles.invert} src={route1} alt="Route1" />
              </a>
            </span>

            <span className={styles.mainJobContent}>
              <p className={styles.jobTitle}>
                <strong>Full Stack Developer</strong> <em>Route1</em> - January
                2017 - March 2017
              </p>

              <p className={styles.smallerDetail}>
                <em>Primary tech:</em> Node.js, Express.js, Bluebird.js, React,
                Ruby on Rails
              </p>
            </span>
          </li>

          <li className={styles.jobWithLogo}>
            <span className={styles.logo}>
              <a
                href="https://www.mbinteractive.co.uk"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={mbinteractive} alt="MB Interactive" />
              </a>
            </span>

            <span className={styles.mainJobContent}>
              <p className={styles.jobTitle}>
                <strong>ASP.NET / C# Developer</strong> <em>MB Interactive</em>{" "}
                - January 2013 - December 2016
              </p>

              <p className={styles.smallerDetail}>
                <em>Primary tech:</em> ASP.NET, Web API 2, ASP.NET MVC, C#, MS
                SQL Server, NHibernate, Entity Framework
              </p>
            </span>
          </li>
        </ul>
      </div>

      <div className={styles.section} data-section="education">
        <h2>Education</h2>

        <ul>
          <li>
            <p className={styles.education}>
              <strong>
                MSc Computer Science - University of Hertfordshire
              </strong>
            </p>
            <p className={styles.smallerDetail}>
              <em>With Distinction</em> - January 2013 - January 2014
            </p>
          </li>

          <li>
            <p className={styles.education}>
              <strong>BA English Literature</strong>
            </p>
            <p className={styles.smallerDetail}>
              <em>2nd Class Honours</em> - September 2008 - July 2011
            </p>
          </li>
        </ul>
      </div>

      <div className={styles.section} data-section="competencies">
        <h2>Primary Skills</h2>

        <p>
          I have worn many hats in my career thus far. I really like
          experimenting with new tech, but I have a few staples that I really
          enjoy and I am the most experienced in. These are listed below.
          <br />
        </p>

        <ul className={styles.skillsList}>
          <li>Ruby on Rails</li>
          <li>RESTful API development</li>
          <li>ES6/ES7 JavaScript</li>
          <li>React / Redux & friends</li>
          <li>
            Webpack config!{" "}
            <span role="img" aria-label="Wooo">
              🎉
            </span>
          </li>
          <li>BDD / TDD (across the stack)</li>
        </ul>
      </div>

      <div className={styles.section} data-section="links">
        <h2>Links</h2>

        <p>
          You can find me on{" "}
          <a className={styles.externalLink} href="https://github.com/adaam2">
            GitHub
          </a>{" "}
          or on{" "}
          <a
            className={styles.externalLink}
            href="https://bitbucket.org/adaam2"
          >
            Bitbucket
          </a>
        </p>
        <p>
          If you want to get in touch regarding a job, then please email me on{" "}
          <a
            className={styles.externalLink}
            href="mailto:hello@adamjamesbull.co.uk"
          >
            hello@adamjamesbull.co.uk
          </a>
          .
        </p>
      </div>
    </div>
  );
};

Container.propTypes = {
  setPageTitle: PropTypes.func.isRequired
};

export default withPageTitle(Container);
