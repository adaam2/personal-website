import React from "react";

const Container = () => (
  <div data-section="not-found">
    <h2>Oops</h2>

    <p>This page is not here anymore!</p>
  </div>
);

export default Container;
