import React from "react";
import Moment from "moment";

import { convertMarkdownToHtml } from "../../shared/markdown";
import styles from "./Post.module.scss";

const Post = ({ data }) => (
  <div data-section="post" className={styles.postContainer}>
    <h3 data-identifier="post-title">
      {data.title}

      <span className={styles.postMeta}>
        {Moment(data.created_at).fromNow()}
      </span>
    </h3>

    <div
      data-identifier="post-body"
      className={styles.postBody}
      dangerouslySetInnerHTML={{
        __html: convertMarkdownToHtml(data.body)
      }}
    />
  </div>
);

export default Post;
