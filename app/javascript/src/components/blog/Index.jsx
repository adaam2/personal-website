import React from "react";
import _ from "lodash";
import ApiClient from "../../shared/api";

import Loader from "../shared/Loader";
import PostPreview from "./PostPreview";

import styles from "./index.module.scss";

export default class Index extends React.Component {
  constructor(props) {
    super(props);

    this.apiClient = new ApiClient();

    this.state = {
      posts: [],
      loading: true
    };
  }

  componentDidMount() {
    document.title = "adamjamesbull.co.uk - Blog";
    this.apiClient.get("/v1/posts").then(this.populatePosts);
  }

  populatePosts = data => {
    this.setState({
      posts: data.body,
      loading: false
    });
  };

  postsList = () => {
    if (this.state.loading) {
      return <Loader />;
    }

    if (!this.state.loading && this.state.posts.length < 1) {
      return (
        <div>
          <h2>Oops</h2>
          <p>No posts here currently!</p>
        </div>
      );
    }

    return _.map(this.state.posts, post => (
      <PostPreview {...this.props} key={post.id} data={post} />
    ));
  };

  render() {
    return (
      <div className={styles.listContainer} data-section="posts">
        {this.postsList()}
      </div>
    );
  }
}
