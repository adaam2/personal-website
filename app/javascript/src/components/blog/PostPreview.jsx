import React from "react";
import { Link } from "react-router-dom";
import Moment from "moment";
import PropTypes from "prop-types";

import classNames from "classnames/bind";

import styles from "./post-preview.module.scss";

const cx = classNames.bind(styles);

export default class PostPreview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hovered: false
    };
  }

  cardContent = () => (
    <div className={styles.cardContent}>
      <span className={styles.postDate}>
        Posted {Moment(this.props.data.created_at).fromNow()}
      </span>
      <Link className={styles.postLink} to={`/blog/${this.props.data.slug}`}>
        <h3>{this.props.data.title}</h3>
      </Link>
    </div>
  );

  handleClick = e => {
    e.stopPropagation();
    this.props.history.push(`/blog/${this.props.data.slug}`);
  };

  render() {
    const classes = cx({
      [styles.preview]: true,
      [styles.hovered]: this.state.hovered
    });
    return (
      <div onClick={this.handleClick} className={classes}>
        <div className={styles.previewInner}>{this.cardContent()}</div>
      </div>
    );
  }
}

PostPreview.propTypes = {
  data: PropTypes.shape({
    slug: PropTypes.string.isRequired,
    created_at: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};
