import React from "react";
import HighlightJs from "highlightjs";
import PropTypes from "prop-types";

import ApiClient from "../../shared/api";
import Post from "./Post";
import Loader from "../shared/Loader";
import withPageTitle from "../shared/hoc/withPageTitle";
import "highlightjs/styles/tomorrow-night-eighties.css";

HighlightJs.configure({ tabReplace: "  " });

class Show extends React.Component {
  constructor(props) {
    super(props);

    this.apiClient = new ApiClient();

    this.state = {
      post: undefined,
      loading: true
    };
  }

  componentDidMount() {
    this.apiClient
      .get(`v1/posts/${this.props.match.params.slug}`)
      .then(this.populatePost);
  }

  populatePost = data => {
    this.setState({
      post: data.body,
      loading: false
    });

    this.props.setPageTitle(data.body.title);

    const codes = document.querySelectorAll("pre, pre>code");
    codes.forEach(code => HighlightJs.highlightBlock(code));
  };

  render() {
    if (this.state.loading) {
      return <Loader />;
    }

    return (
      <div>
        <Post data={this.state.post} />
      </div>
    );
  }
}

Show.propTypes = {
  setPageTitle: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

export default withPageTitle(Show);
