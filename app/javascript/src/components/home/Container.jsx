import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import classNames from "classnames/bind";

import ApiClient from "../../shared/api";

import withPageTitle from "../shared/hoc/withPageTitle";
import Loader from "../shared/Loader";
import styles from "./home.module.scss";

const cx = classNames.bind(styles);

class Container extends React.Component {
  constructor(props) {
    super(props);
    this.apiClient = new ApiClient();
    this.state = { loadingPosts: false, posts: [] };
  }

  componentDidMount() {
    this.props.setPageTitle("Home");
    this.apiClient.get("/v1/posts?limit=5").then(this.populatePosts);
  }

  populatePosts = data => {
    this.setState({
      posts: data.body,
      loadingPosts: false
    });
  };

  posts = () => {
    if (this.state.loadingPosts) {
      return <Loader />;
    }

    return this.state.posts.map(post => (
      <li key={post.id} className={styles.post}>
        <Link className={styles.externalLink} to={`/blog/${post.slug}`}>
          {post.title}
        </Link>
      </li>
    ));
  };

  sectionClasses = (isCallout = false) =>
    cx({
      [styles.section]: true,
      [styles.calloutSection]: isCallout,
      [styles.normalSection]: !isCallout
    });

  render() {
    return (
      <div className={styles.sectionContainer}>
        <div className={this.sectionClasses(true)}>
          <h2>A little about me</h2>

          <p>
            My name&apos;s Adam. I&apos;m a full-stack developer. I always like
            to experiment with new things; my ethos is to leave no stone
            unturned. I very much hold the polyglot developer mindset, and I
            think it’s important to have wide-ranging knowledge in a variety of
            programming languages and technologies.
          </p>
        </div>

        <div className={this.sectionClasses()}>
          <h2>Recent posts</h2>

          <ul className={styles.postList}>{this.posts()}</ul>
        </div>
      </div>
    );
  }
}

Container.propTypes = {
  setPageTitle: PropTypes.func.isRequired
};

export default withPageTitle(Container);
