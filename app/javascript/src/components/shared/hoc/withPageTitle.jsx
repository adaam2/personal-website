import React from "react";

const withPageTitle = Component => {
  class WithPageTitle extends React.Component {
    setPageTitle = title => {
      document.title = `adamjamesbull.co.uk - ${title}`;
    };

    render() {
      return <Component setPageTitle={this.setPageTitle} {...this.props} />;
    }
  }

  return WithPageTitle;
};

export default withPageTitle;
