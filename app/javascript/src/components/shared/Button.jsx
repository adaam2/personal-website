import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";
import styles from "./button.module.scss";

const cx = classNames.bind(styles);

const Button = ({ children, classes }) => {
  const extraClasses = cx({
    [styles.button]: true,
    classes
  });

  return <div className={extraClasses}>{children}</div>;
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  classes: PropTypes.string
};

Button.defaultProps = {
  classes: ""
};

export default Button;
