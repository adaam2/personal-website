import React from "react";
import PropTypes from "prop-types";
import styles from "./alert.module.scss";

export default class Alert extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props;
  }

  componentWillReceiveProps(nextProps) {
    // reset the timer if children are changed
    if (nextProps.children !== this.props.children) {
      this.setTimer();
      this.setState({ visible: true });
    }
  }

  componentDidMount() {
    this.setTimer();
  }

  setTimer() {
    // clear any existing timer
    if (this._timer != null) {
      clearTimeout(this._timer);
    }

    // hide after `delay` milliseconds
    this._timer = setTimeout(() => {
      this.setState({ visible: false });
      this._timer = null;
      this.props.onExit();
    }, this.props.delay);
  }

  componentWillUnmount() {
    clearTimeout(this._timer);
  }

  render() {
    return this.state.visible ? (
      <div className={styles.alert}>{this.props.children}</div>
    ) : (
      <span />
    );
  }
}

Alert.propTypes = {
  onExit: PropTypes.func,
  delay: PropTypes.number.isRequired,
  children: PropTypes.any.isRequired
};

Alert.defaultProps = {
  onExit: () => {}
};
