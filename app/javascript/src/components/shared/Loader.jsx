import React from "react";
import Loader from "react-loader-spinner";

const LoadingSpinner = () => <Loader type="ThreeDots" color="#37B3BF" height={50} width={50} />;

export default LoadingSpinner;
