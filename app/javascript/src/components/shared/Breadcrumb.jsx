import _ from "lodash";
import React from "react";
import PropTypes from "prop-types";

import styles from "./breadcrumb.module.scss";

const Breadcrumb = props => {
  const { pathname } = props.location;

  if (pathname === "/" || pathname === "") {
    return null;
  }

  const iteratePathParts = () => {
    const pathParts = _.compact(pathname.split("/"));

    // eslint-disable-next-line prefer-const
    let crumbs = [];

    pathParts.reduce((prev, current, index) => {
      const pathSection = `${prev}/${current}`;
      const prevUrl = `/${prev}`;
      const url = `/${pathSection}`;

      crumbs.push(
        <div>
          <a className={styles.breadcrumbItem} href={prevUrl}>
            {prev}
          </a>
          {index !== 0 ? <span className={styles.divider}>­­»</span> : ""}

          <a className={styles.breadcrumbItem} href={url}>
            {current}
          </a>
        </div>
      );

      return pathSection;
    });

    return crumbs;
  };
  const parts = iteratePathParts();

  if (!parts.length) {
    return null;
  }
  return <div className={styles.breadcrumb}>{iteratePathParts()}</div>;
};

Breadcrumb.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
};

export default Breadcrumb;
