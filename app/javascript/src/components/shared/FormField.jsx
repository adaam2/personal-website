import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import styles from "./form-field.module.scss";

export default class FormField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name
    };
  }

  onChange = e => {
    this.props.onChange({
      name: this.state.name,
      value: e.currentTarget.value
    });
  };

  errors = () => {
    if (this.props.errors.length < 1) {
      return null;
    }

    const allErrors = Object.entries(this.props.errors).map(([key, value]) => ({
      key,
      value
    }));

    const forField = _.filter(
      allErrors,
      error => error.key === this.props.name
    );

    return _.map(forField, error =>
      _.map(error.value, v => <div className={styles.error}>{v}</div>)
    );
  };

  render() {
    if (this.props.children) {
      return (
        <div className={styles.fieldContainer}>
          <label id={this.props.name} htmlFor={this.state.name}>
            <div className={styles.label}>{this.props.name}</div>
            <div className={styles.inputContainer}>{this.props.children}</div>
          </label>

          {this.errors()}
        </div>
      );
    }
    return (
      <div className={styles.fieldContainer}>
        <label className={styles.label} htmlFor={this.state.name}>
          {this.props.name}
        </label>
        <span className={styles.inputContainer}>
          <input
            type={this.props.type}
            defaultValue={this.props.value}
            onChange={this.onChange}
          />
        </span>

        {this.errors()}
      </div>
    );
  }
}

FormField.propTypes = {
  onChange: PropTypes.func.isRequired,
  children: PropTypes.any,
  name: PropTypes.string.isRequired
};
