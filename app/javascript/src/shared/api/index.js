import request from "superagent";
import { logAction } from "../logging";

export default class ApiClient {
  constructor(
    baseUri = "/api",
    authenticationToken = "",
    handleError = () => {}
  ) {
    this.baseUri = baseUri;
    this.handleError = handleError;
    this.authenticationToken = authenticationToken;
  }

  get(path, headers = {}, params = {}) {
    return this.sendRequest("GET", path, headers, params);
  }

  post(path, headers = {}, body = {}) {
    return this.sendRequest("POST", path, headers, body);
  }

  delete(path, headers = {}, params = {}) {
    return this.sendRequest("DELETE", path, headers, params);
  }

  patch(path, headers = {}, body = {}) {
    return this.sendRequest("PATCH", path, headers, body);
  }

  requestUrl(path) {
    return `${this.baseUri}/${path}`;
  }

  sendRequest(method, path, headers = {}, params = {}) {
    if (this.authenticationToken) {
      logAction(
        `Made request to ${method.toUpperCase()} ${path} using ${
          this.authenticationToken
        }`
      );
    }

    return request(method, this.requestUrl(path))
      .send(params)
      .on("error", this.handleError)
      .set({ ...this.requestHeaders(), ...headers });
  }

  requestHeaders() {
    return {
      Authorization: this.authenticationToken
    };
  }
}
