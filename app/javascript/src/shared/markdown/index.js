import showdown from "showdown";

export const convertMarkdownToHtml = markdown => {
  const converter = new showdown.Converter();
  return converter.makeHtml(markdown);
};
