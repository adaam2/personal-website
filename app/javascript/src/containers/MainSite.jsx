import React from "react";
import { Route } from "react-router-dom";
import classNames from "classnames/bind";

import "../shared/fonts/hk-grotesk/HKGrotesk-Medium.otf";

import Nav from "./Nav";
import Breadcrumb from "../components/shared/Breadcrumb";

import styles from "./MainSite.module.scss";

const cx = classNames.bind(styles);

const MainSiteLayout = ({ component: Component, ...rest }) => {
  const mainClasses = cx({
    [styles.container]: true,
    [styles.mainSection]: true,
    [styles.sensibleContainer]: true
  });

  return (
    <Route
      {...rest}
      render={matchProps => (
        <div className={styles.mainSiteContainer}>
          <header className={styles.header}>
            <Nav />
          </header>

          <main className={mainClasses}>
            <Breadcrumb {...rest} />
            <Component {...matchProps} />
          </main>
        </div>
      )}
    />
  );
};

export default MainSiteLayout;
