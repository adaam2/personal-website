import React from "react";
import classNames from "classnames";
import { NavLink } from "react-router-dom";

import styles from "./Nav.module.scss";

const cx = classNames.bind(styles);

const Nav = () => {
  const navClasses = cx({
    [styles.nav]: true,
    [styles.sensibleContainer]: true
  });

  return (
    <div className={navClasses}>
      <div className={styles.navBrand}>
        <h1>ajb.co.uk</h1>
      </div>

      <ul className={styles.navList}>
        <li className={styles.navItem}>
          <NavLink exact to="/">
            Home
          </NavLink>
        </li>
        <li className={styles.navItem}>
          <NavLink to="/blog">Blog</NavLink>
        </li>
        <li className={styles.navItem}>
          <NavLink exact to="/cv">
            Curriculum Vitae
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default Nav;
