import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import "@babel/polyfill";
import MainSiteLayout from "../src/containers/MainSite.jsx";
import Home from "../src/components/home/Container.jsx";
import BlogIndex from "../src/components/blog/Index.jsx";
import BlogShow from "../src/components/blog/Show.jsx";
import CurriculumVitae from "../src/components/cv/Container.jsx";
import NotFound from "../src/components/notFound/Container.jsx";

const supportsHistory = "pushState" in window.history;

const Site = () => (
  <Router forceRefresh={!supportsHistory}>
    <Switch>
      <MainSiteLayout exact path="/" component={Home} />
      <MainSiteLayout exact path="/blog" component={BlogIndex} />
      <MainSiteLayout exact path="/blog/:slug" component={BlogShow} />
      <MainSiteLayout exact path="/cv" component={CurriculumVitae} />
      <MainSiteLayout component={NotFound} status={404} />
    </Switch>
  </Router>
);

document.addEventListener("DOMContentLoaded", () => {
  ReactDOM.render(Site(), document.getElementById("app"));
});
