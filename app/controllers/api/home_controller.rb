class Api::HomeController < Api::BaseController
  def index
    render json: { status: :ok }
  end
end
