class Api::V1::PostsController < Api::BaseController  
  def index
    posts = Post.published.order(created_at: :desc)
    posts = posts.limit(limit) if limit

    render json: posts
  end

  def show
    render json: post
  end

  private
 
  def limit
    params[:limit]
  end

  def post
    Post.find_by(slug: params[:slug])
  end
end
