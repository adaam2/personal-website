class Post < ApplicationRecord
  # Callbacks
  after_create :generate_slug
  before_save :populate_is_published
  
  # Validations
  validates :title, :body, presence: true
  validates :title, uniqueness: true
  validates :title, length: { minimum: 20, maximum: 500 }

  # Scopes  
  scope :published, -> { where(is_published: true) }

  private

  def generate_slug
    update(slug: title&.parameterize)
  end

  def populate_is_published
    self.is_published = self.is_published.present? ? self.is_published : false
    true
  end
end
