require 'rails_helper'

RSpec.describe PostSerializer, type: :serializer do
  let(:post) { create :post }
  subject { described_class.new(post).serializable_hash }

  describe 'Payload keys' do
    it 'should serialize the correct attributes' do
      expect(subject.keys)
        .to match_array(
          %I[
            id
            title
            body
            created_at
            slug
            is_published
          ]
        )
    end
  end
end
