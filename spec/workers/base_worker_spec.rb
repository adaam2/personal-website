require 'rails_helper'

RSpec.describe BaseWorker, type: :worker do
  describe 'perform' do
    it 'raises' do
      expect {
        described_class.new.perform
      }.to raise_error(NotImplementedError)
    end
  end
end
