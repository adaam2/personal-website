require "rails_helper"

RSpec.feature "Loading the blog listing page", :type => :feature do
  let!(:posts) { create_list(:post, 5) }

  scenario "Loading the blog index" do
    visit "/blog"

    posts.each do |post|
      expect(page).to have_text(post.title)
    end
  end
end