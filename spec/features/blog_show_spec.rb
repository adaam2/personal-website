require "rails_helper"

RSpec.feature "Loading the blog show page", :type => :feature do
  let!(:post) { create(:post, created_at: Time.now - 1.day) }

  scenario "Loading the blog show page" do
    visit "/blog/#{post.slug}"

    expect(page).to have_text(post.title)
    expect(page).to have_text(post.body)
    expect(page).to have_text('a day ago')
  end
end