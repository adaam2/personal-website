require "rails_helper"

RSpec.feature "Loading the home page", :type => :feature do
  let!(:new_posts) { create_list(:post, 5, created_at: Time.now - 1.day) }
  let!(:old_posts) { create_list(:post, 2, created_at: Time.now - 2.years) }

  scenario "It shows the 5 most recent post titles" do
    visit "/"

    new_posts.each do |post|
      expect(page).to have_link(post.title)
    end

    old_posts.each do |post|
      expect(page).not_to have_link(post.title)
    end
  end
end