require 'rails_helper'

RSpec.describe AppController, type: :controller do
  describe 'index' do
    it 'returns a OK status code' do
      get :index

      expect(response).to be_ok
    end
  end
end
