require 'rails_helper'

RSpec.describe Api::V1::PostsController, type: :controller do
  describe 'index' do
    let!(:published) { create :post, :published, created_at: Time.now }
    let!(:another_published) { create :post, :published, created_at: Time.now - 1.day }
    let!(:unpublished) { create :post, :unpublished }

    it 'only serializes the published posts' do
      get :index

      json = JSON.parse(response.body)
      ids = json.map { |post| post['id'] }

      expect(ids).to match [published.id, another_published.id]
    end

    context 'When there is a limit' do
      it 'only returns up to the limit returning the most recent post' do
        get :index, params: { limit: 1 }

        json = JSON.parse(response.body)
        ids = json.map { |post| post['id'] }

        expect(ids).to eq [published.id]
      end
    end
  end

  describe 'show' do
    let!(:post) { create :post, :published, title: 'This is a post title that is long enough' }

    it 'serializes the correct post' do
      get :show, params: { slug: post.slug }

      json = JSON.parse(response.body)

      expect(json.dig('title'))
        .to eq 'This is a post title that is long enough'
    end
  end
end
