require 'rails_helper'

RSpec.describe Api::HomeController, type: :controller do
  describe 'index' do
    let(:json) { JSON.parse(response.body) }

    it 'returns an OK status' do
      get :index
      
      expect(json['status']).to eq 'ok'
    end
  end
end
