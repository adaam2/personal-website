require 'rails_helper'

RSpec.describe Post, type: :model do
  describe 'Validations' do
    it { should validate_presence_of(:title) }
    
    it do
      should validate_length_of(:title)
        .is_at_least(20)
        .is_at_most(500)
    end
  end

  describe 'Slug generation' do
    let!(:post) { create :post, slug: nil, title: 'A post with a long enough title' }

    it 'generates the slug' do
      expect(post.slug).to eq 'a-post-with-a-long-enough-title'
    end
  end
end
